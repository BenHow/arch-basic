# Arch Basic Install Commands-Script

## WARNING

_I haven't tried (yet) those scripts. So, be careful and report any bugs !_

This work is based upon and forked from [Ermanno Ferrari's](https://eflinux.com/) ARCH BASIC INSTALL PUBLIC scripts,
available on this (Gitlab repository)[https://gitlab.com/eflinux/arch-basic].
It's also based upon [ChrisTitusTech's ArchMatic](https://github.com/ChrisTitusTech/ArchMatic)
In this repository you will find packages-scripts for the base install of Arch Linux (EFI only) and the Gnome, KDE, Cinnamon, Mate and Xfce desktop environments.
Modify the packages to your liking, make the script executable with chmod +x scriptname and then run with ./scriptname.
Remember that the first part of the Arch Linux install is manual, that is you will have to partition, format and mount the disk yourself. Install the base packages and make sure to inlcude git so that you can clone the repository in chroot.

A small summary:

1. If needed, load your keymap
2. If needed, configure your WiFi connection (with iwctl)
3. This step installs Arch Linux to your hard drive (EFI, ). _IT WILL FORMAT THE DISK_

```bash
wget https://gitlab.com/BenHow/arch-basic/-/raw/master/preinstall.sh?inline=false
sh preinstall.sh
reboot
```

## Arch Linux First Boot

```bash
pacman -S --noconfirm pacman-contrib curl git
git clone https://gitlab.com/BenHow/arch-basic
cd archbasic
chmod +x install-base.sh
./install-base.sh
```

Choose your Desktop Environment by repeating the step above with the script gnome.sh , cinnamon.sh , kde.sh ...
