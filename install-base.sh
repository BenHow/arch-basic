#!/usr/bin/env bash

if ! source install.conf; then
	read -p "Please enter hostname:" hostname

	read -p "Please enter username:" username

	read -sp "Please enter password:" password

	read -sp "Please repeat password:" password2

	# Check both passwords match
	if [ "$password" != "$password2" ]; then
	    echo "Passwords do not match"
	    exit 1
	fi
  printf "hostname="$hostname"\n" >> "install.conf"
  printf "username="$username"\n" >> "install.conf"
  printf "password="$password"\n" >> "install.conf"
fi

echo "-------------------------------------------------"
echo "Setting up mirrors for optimal download - FR Only"
echo "-------------------------------------------------"
pacman -S --noconfirm pacman-contrib curl
mv /etc/pacman.d/mirrorlist /etc/pacman.d/mirrorlist.backup
curl -s "https://www.archlinux.org/mirrorlist/?country=FR&protocol=https&use_mirror_status=on" | sed -e 's/^#Server/Server/' -e '/^#/d' | rankmirrors -n 5 - > /etc/pacman.d/mirrorlist

nc=$(grep -c ^processor /proc/cpuinfo)
echo "You have " $nc" cores."
echo "-------------------------------------------------"
echo "Changing the makeflags for "$nc" cores."
sudo sed -i 's/#MAKEFLAGS="-j2"/MAKEFLAGS="-j$nc"/g' /etc/makepkg.conf
echo "Changing the compression settings for "$nc" cores."
sudo sed -i 's/COMPRESSXZ=(xz -c -z -)/COMPRESSXZ=(xz -c -T $nc -z -)/g' /etc/makepkg.conf

echo "-------------------------------------------------"
echo "       Setup Language to FR and set locale       "
echo "-------------------------------------------------"
sed -i '/fr_FR.UTF-8 UTF-8/s/^#//g' /etc/locale.gen
locale-gen
timedatectl --no-ask-password set-timezone Europe/Paris
timedatectl --no-ask-password set-ntp 1
localectl --no-ask-password set-locale LANG="fr_FR.UTF-8" LANGUAGE="fr_FR:en_US" LC_COLLATE="C" LC_TIME="fr_FR.UTF-8"

# Set keymaps
localectl --no-ask-password set-keymap fr

# Hostname
hostnamectl --no-ask-password set-hostname $hostname

# Add sudo no password rights
sed -i 's/^# %wheel ALL=(ALL) NOPASSWD: ALL/%wheel ALL=(ALL) NOPASSWD: ALL/' /etc/sudoers



# read -p 'enter your username : ' username
# read -p 'enter your password : ' -s password
# read -p 'enter your machine name : ' machinename

# ln -sf /usr/share/zoneinfo/Europe/Paris /etc/localtime
# hwclock --systohc
# sed -i '/fr_FR.UTF-8 UTF-8/s/^#//g' /etc/locale.gen
# locale-gen
# locale='LANG="fr_FR.UTF-8"
# LANGUAGE="fr_FR:en_US"
# LC_COLLATE=C'
# echo "$locale" > /etc/locale.conf
# echo "KEYMAP=fr" >> /etc/vconsole.conf
# echo "$machinename" >> /etc/hostname
# echo "127.0.0.1 localhost" >> /etc/hosts
# echo "::1       localhost" >> /etc/hosts
# echo "127.0.1.1 $machinename.localdomain $machinename" >> /etc/hosts
# echo root:$password | chpasswd

# You can add xorg to the installation packages, I usually add it at the DE or WM install script
# You can remove the tlp package if you are installing on a desktop or vm

pacman -S --needed networkmanager network-manager-applet dialog wpa_supplicant mtools dosfstools base-devel linux-headers xdg-user-dirs xdg-utils inetutils bluez bluez-utils cups alsa-utils pipewire pipewire-alsa pipewire-pulse pipewire-jack bash-completion reflector acpi git flatpak acpid terminus-font

systemctl enable NetworkManager
# systemctl enable bluetooth
# systemctl enable cups
systemctl enable reflector.timer
systemctl enable fstrim.timer

systemctl enable acpid

useradd -m $username
echo $username:$password | chpasswd

echo "$username ALL=(ALL) ALL" >> /etc/sudoers.d/$username


printf "\e[1;32mDone! Type exit, umount -a and reboot.\e[0m"




