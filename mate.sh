#!/bin/bash

sudo timedatectl set-ntp true
sudo hwclock --systohc
sudo setfont ter-132n

sudo reflector -c France -a 6 --sort rate --save /etc/pacman.d/mirrorlist


git clone https://aur.archlinux.org/yay.git
cd yay/
makepkg -si --noconfirm

yay -S --noconfirm zramd

yay -S --noconfirm auto-cpufreq
sudo systemctl enable --now auto-cpufreq
sudo systemctl enable --now zramd.service

sudo pacman -Syy

sudo pacman -S --needed xorg wget lightdm lightdm-gtk-greeter lightdm-gtk-greeter-settings mate mate-extra mate-control-center firefox vlc chromium scummvm code intellij-idea-community-edition focuswriter nodejs supertux supertuxkart 0ad libreoffice-still-fr htop whois neofetch linux-lts firefox-i18n-fr thunderbird-i18n-fr ttf-bitstream-vera ttf-croscore ttf-dejavu ttf-droid gnu-free-fonts ttf-ibm-plex ttf-liberation ttf-linux-libertine noto-fonts ttf-roboto ttf-ubuntu-font-family ttf-jetbrains-mono


# sudo flatpak install -y spotify
# sudo flatpak install -y kdenlive

yay -S --noconfirm pamac-all

yay -S --noconfirm timeshift timeshift-autosnap archlinux-appstream-data-pamac

sudo systemctl enable lightdm
/bin/echo -e "\e[1;32mREBOOTING IN 5..4..3..2..1..\e[0m"
sleep 5
sudo reboot
