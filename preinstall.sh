#!/usr/bin/env bash


# 3 - install git : pacman -S git
# 4 - grab this script : https://gitlab.com/BenHow/arch-basic/-/raw/master/preinstall.sh?inline=false
# 5 - run ./preinstall.sh

echo "-------------------------------------------------"
echo "Setting up mirrors for optimal download - FR Only"
echo "-------------------------------------------------"
timedatectl set-ntp true
pacman -S --noconfirm pacman-contrib
mv /etc/pacman.d/mirrorlist /etc/pacman.d/mirrorlist.backup
curl -s "https://www.archlinux.org/mirrorlist/?country=FR&protocol=https&use_mirror_status=on" | sed -e 's/^#Server/Server/' -e '/^#/d' | rankmirrors -n 5 - > /etc/pacman.d/mirrorlist


echo -e "\nInstalling disk formatting tools...\n$HR"
pacman -S --noconfirm gptfdisk btrfs-progs

echo "-------------------------------------------------"
echo "-------select your disk to format----------------"
echo "-------------------------------------------------"
lsblk
echo "Please enter disk: (example : /dev/sda)"
read DISK
echo "--------------------------------------"
echo -e "\nFormatting disk...\n(450M for the boot partition at the beginning,\n550M for empty space at the end of the disk [assuming it's an SSD]\n and between the two the root partition)\n$HR"
echo "--------------------------------------"


# disk prep
sgdisk -Z ${DISK} # zap all on disk
sgdisk -a 2048 -o ${DISK} # new gpt disk 2048 alignment

sgdisk -n 1:0:+450M -t 1:ef00 -c 1:"EFI" ${DISK}
sgdisk -n 2:0:-550M -t 2:8300 -c 2:"ROOT" ${DISK}

mkfs.vfat -F32 -n "BOOT" "${DISK}1"
mkfs.btrfs -L "ROOT" "${DISK}2" -f 

mount -t btrfs "${DISK}2" /mnt
cd /mnt
btrfs subvolume create @
btrfs subvolume create @home
btrfs subvolume create @var
cd
umount /mnt
mount -o noatime,compress=zstd,space_cache,discard=async,subvol=@ ${DISK}2 /mnt
mkdir /mnt/{boot,home,var}
mount -o noatime,compress=zstd,space_cache,discard=async,subvol=@home ${DISK}2 /mnt/home
mount -o noatime,compress=zstd,space_cache,discard=async,subvol=@var ${DISK}2 /mnt/var
mount -t vfat "${DISK}1" /mnt/boot/

echo "--------------------------------------"
echo "-- Arch Install on Main Drive       --"
echo "--------------------------------------"

pacstrap /mnt base linux linux-firmware git nano intel-ucode btrfs-progs --noconfirm --needed
genfstab -U /mnt >> /mnt/etc/fstab

arch-chroot /mnt

echo "--------------------------------------"
echo "-- Bootloader Systemd Installation  --"
echo "--------------------------------------"
pacman -S --needed --noconfirm grub grub-btrfs efibootmgr
grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=GRUB
grub-mkconfig -o /boot/grub/grub.cfg

echo "--------------------------------------"
echo "--      Set Password for Root       --"
echo "--------------------------------------"
echo "Enter password for root user: "
passwd root

exit
umount -R /mnt

echo "--------------------------------------"
echo "--   SYSTEM READY FOR FIRST BOOT    --"
echo "--------------------------------------"
