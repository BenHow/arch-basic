#!/bin/bash

sudo timedatectl set-ntp true
sudo hwclock --systohc
sudo setfont ter-132n

sudo reflector -c France -a 6 --sort rate --save /etc/pacman.d/mirrorlist


git clone https://aur.archlinux.org/yay.git
cd yay/
makepkg -si --noconfirm

yay -S --noconfirm zramd

yay -S --noconfirm auto-cpufreq
sudo systemctl enable --now auto-cpufreq
sudo systemctl enable --now zramd.service

sudo pacman -Syy

sudo pacman -S --needed gdm gnome wget intellij-idea-community-edition focuswriter nodejs supertux supertuxkart papirus-icon-theme 0ad code chromium ffmpegthumbnailer libreoffice-still-fr htop whois neofetch gufw linux-lts firefox-i18n-fr gnome-tweaks scummvm thunderbird-i18n-fr arc-gtk-theme arc-icon-theme vlc ttf-bitstream-vera ttf-croscore ttf-dejavu ttf-droid gnu-free-fonts ttf-ibm-plex ttf-liberation ttf-linux-libertine noto-fonts ttf-roboto ttf-ubuntu-font-family ttf-jetbrains-mono gst-plugins-bad gst-plugins-base gst-plugins-good gst-plugins-ugly gst-libav materia-gtk-theme arc-solid-gtk-theme adapta-gtk-theme



# sudo flatpak install -y skype
# sudo flatpak install -y kdenlive
# sudo flatpak install -y digikam

yay -S --noconfirm pamac-all

sudo pacman -Rs --noconfirm epiphany gnome-books gnome-boxes gnome-software gnome-maps 

yay -S --noconfirm timeshift timeshift-autosnap chrome-gnome-shell appimagelauncher archlinux-appstream-data-pamac

sudo pacman -S --needed zsh zsh-completions zsh-autosuggestions zsh-syntax-highlighting ttf-nerd-fonts-symbols
yay -S --noconfirm pamac-zsh-completions zsh-theme-powerlevel10k-git
chsh -s /bin/zsh
echo 'source /usr/share/zsh-theme-powerlevel10k/powerlevel10k.zsh-theme' >>~/.zshrc


sudo systemctl enable gdm
sudo systemctl enable ufw.service
/bin/echo -e "\e[1;32mREBOOTING IN 5..4..3..2..1..\e[0m"
sleep 5
sudo reboot
